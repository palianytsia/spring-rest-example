package com.visualmeta.rest;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.visualmeta.rest.model.Product;
import com.visualmeta.rest.model.Shop;
import com.visualmeta.rest.repository.ProductRepository;
import com.visualmeta.rest.repository.ShopRepository;

@Component
public class DataImport implements CommandLineRunner {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ShopRepository shopRepository;

    @Override
    public void run(String... args) throws Exception {
	Shop sOliver = new Shop("s.Oliver", "http://www.ladenzeile.de/s-oliver/");
	shopRepository.save(sOliver);

	List<Product> sOliverProducts = new LinkedList<>();
	sOliverProducts.add(new Product("Cocktailkleid / festliches Kleid rose taupe", 139.95));
	sOliverProducts.add(new Product("Longbluse weiss", 39.99));
	sOliverProducts.forEach(p -> p.shopId = sOliver.id);
	productRepository.save(sOliverProducts);

	Shop nike = new Shop("Nike", "http://www.ladenzeile.de/nike/");
	shopRepository.save(nike);

	List<Product> nikeProducts = new LinkedList<>();
	nikeProducts.add(new Product("Nike Air Max 90 Mesh (gs)", 69.95));
	nikeProducts.add(new Product("Nike Air Max Command GS Sneaker", 89.99));
	nikeProducts.forEach(p -> p.shopId = nike.id);
	productRepository.save(nikeProducts);
    }

}
