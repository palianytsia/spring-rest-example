package com.visualmeta.rest;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.visualmeta.rest.web.ApiMonitor;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
	registry.addInterceptor(new ApiMonitor());
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
	FilterRegistrationBean registration = new FilterRegistrationBean();
	registration.setFilter(new HiddenHttpMethodFilter());
	registration.setDispatcherTypes(EnumSet.allOf(DispatcherType.class));
	registration.addUrlPatterns("/*");
	return registration;
    }

}
