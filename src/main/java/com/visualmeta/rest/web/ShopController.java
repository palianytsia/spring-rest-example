package com.visualmeta.rest.web;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.visualmeta.rest.model.Shop;
import com.visualmeta.rest.repository.ShopRepository;

@RestController
@RequestMapping(value = "/shops")
public class ShopController extends ApiController {

    private final ShopRepository shopRepository;

    private final ShopResourceAssembler shopResourceAssembler;

    @Autowired
    public ShopController(ShopRepository shopRepository, ShopResourceAssembler shopResourceAssembler) {
	this.shopRepository = shopRepository;
	this.shopResourceAssembler = shopResourceAssembler;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{shopId}")
    @ResponseStatus(HttpStatus.OK)
    public Resource<Shop> getShop(@PathVariable long shopId) {
	Shop shop = shopRepository.findOne(shopId);
	if (shop == null) {
	    throw new NoSuchElementException("Shop with id " + shopId + " does not exist");
	}
	return shopResourceAssembler.toResource(shop);
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    @ResponseStatus(HttpStatus.OK)
    public Resources<Resource<Shop>> listShops() {
	Resources<Resource<Shop>> resources = shopResourceAssembler.toResources(shopRepository.findAll());
	resources.add(linkTo(methodOn(getClass()).listShops()).withSelfRel());
	return resources;
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @ResponseStatus(HttpStatus.CREATED)
    public HttpHeaders createShop(@RequestBody Shop shop) {
	shopRepository.save(shop);
	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(linkTo(methodOn(getClass()).getShop(shop.id)).toUri());
	return headers;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{shopId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateShop(@PathVariable long shopId, @RequestBody Shop shop) {
	shop.id = shopId;
	shopRepository.save(shop);
    }


}
