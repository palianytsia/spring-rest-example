package com.visualmeta.rest.web;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.visualmeta.rest.model.Product;
import com.visualmeta.rest.repository.ProductRepository;

@RestController
@RequestMapping(value = "/products")
public class ProductController extends ApiController {

    private final ProductRepository productRepository;

    private final ProductResourceAssembler productResourceAssembler;

    @Autowired
    public ProductController(ProductRepository productRepository, ProductResourceAssembler productResourceAssembler) {
	this.productRepository = productRepository;
	this.productResourceAssembler = productResourceAssembler;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public Resource<Product> getProduct(@PathVariable long productId) {
	Product product = productRepository.findOne(productId);
	if (product == null) {
	    throw new NoSuchElementException("Product with id " + product + " does not exist");
	}
	return productResourceAssembler.toResource(product);
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    @ResponseStatus(HttpStatus.OK)
    public Resources<Resource<Product>> listProducts(@RequestParam(value = "shop_id", defaultValue = "0") long shopId) {
	Iterable<Product> products;
	if (shopId > 0) {
	    products = productRepository.findByShopId(shopId);
	} else {
	    products = productRepository.findAll();
	}
	return productResourceAssembler.toResources(products);
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @ResponseStatus(HttpStatus.CREATED)
    public HttpHeaders createProduct(@RequestParam(value = "shop_id") long shopId, @RequestBody Product product) {
	product.shopId = shopId;
	productRepository.save(product);
	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(linkTo(methodOn(getClass()).getProduct(product.id)).toUri());
	return headers;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{productId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateProduct(@PathVariable long shopId, @PathVariable long productId, @RequestBody Product product) {
	product.id = productId;
	product.shopId = shopId;
	productRepository.save(product);
    }

}
