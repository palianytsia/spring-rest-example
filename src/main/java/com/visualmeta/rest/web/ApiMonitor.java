package com.visualmeta.rest.web;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class ApiMonitor implements HandlerInterceptor {

    private final Logger log = Logger.getLogger(getClass());

    private final ConcurrentMap<HttpServletRequest, Instant> requestTime = new ConcurrentHashMap<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	requestTime.put(request, Instant.now());
	return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	Duration duration = Duration.between(requestTime.remove(request), Instant.now());
	log.info("Response took " + duration.toMillis() + "ms, response code: " + response.getStatus());
    }
}