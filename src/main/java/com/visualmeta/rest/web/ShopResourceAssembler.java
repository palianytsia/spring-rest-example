package com.visualmeta.rest.web;

import static java.util.stream.Collectors.*;
import static java.util.stream.StreamSupport.*;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import com.visualmeta.rest.model.Shop;

@Component
public class ShopResourceAssembler implements ResourceAssembler<Shop, Resource<Shop>> {

    @Override
    public Resource<Shop> toResource(Shop shop) {
	Resource<Shop> resource = new Resource<Shop>(shop);
	resource.add(linkTo(methodOn(ProductController.class).listProducts(shop.id)).withRel("products"));
	resource.add(linkTo(methodOn(ShopController.class).getShop(shop.id)).withSelfRel());
	return resource;
    }

    public Resources<Resource<Shop>> toResources(Iterable<Shop> shops) {
	return new Resources<>(stream(shops.spliterator(), false).map(this::toResource).collect(toList()));
    }

}
