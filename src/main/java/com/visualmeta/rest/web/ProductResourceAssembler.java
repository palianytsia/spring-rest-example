package com.visualmeta.rest.web;

import static java.util.stream.Collectors.*;
import static java.util.stream.StreamSupport.*;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import com.visualmeta.rest.model.Product;

@Component
public class ProductResourceAssembler implements ResourceAssembler<Product, Resource<Product>> {

    @Override
    public Resource<Product> toResource(Product product) {
	Resource<Product> resource = new Resource<Product>(product);
	resource.add(linkTo(methodOn(ShopController.class).getShop(product.shopId)).withRel("shop"));
	resource.add(linkTo(methodOn(ProductController.class).getProduct(product.id)).withSelfRel());
	return resource;
    }

    public Resources<Resource<Product>> toResources(Iterable<Product> products) {
	return new Resources<>(stream(products.spliterator(), false).map(this::toResource).collect(toList()));
    }

}
