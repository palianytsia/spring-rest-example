package com.visualmeta.rest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    public long id;

    public String name;

    public String siteUrl;

    public Shop() {
    }

    public Shop(String name, String siteUrl) {
	this.name = name;
	this.siteUrl = siteUrl;
    }

    @Override
    public String toString() {
	return name + " (" + id + ")";
    }
}
