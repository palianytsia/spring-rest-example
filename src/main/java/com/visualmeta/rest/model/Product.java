package com.visualmeta.rest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    public long id;

    public String name;

    public Double price;

    @JsonIgnore
    public long shopId;

    public Product() {
    }

    public Product(String name, Double price) {
	this.name = name;
	this.price = price;
    }

    @Override
    public String toString() {
	return name + " (" + id + ")";
    }
}
