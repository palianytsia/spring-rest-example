package com.visualmeta.rest.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.visualmeta.rest.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByShopId(long shopId);

}
