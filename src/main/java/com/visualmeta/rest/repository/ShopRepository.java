package com.visualmeta.rest.repository;

import org.springframework.data.repository.CrudRepository;

import com.visualmeta.rest.model.Shop;

public interface ShopRepository extends CrudRepository<Shop, Long> {

}
